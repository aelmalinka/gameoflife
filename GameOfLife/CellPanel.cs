﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GameOfLife
{
	class CellPanel : GraphicsPanel
	{
		/// <summary>
		/// Construct a new Default CellPanel
		/// </summary>
		public CellPanel()
			: base()
		{
			Generation = 0;

			WrapEdges = true;
			DrawNumbers = true;
			DrawLines = true;
			DrawHUD = false;
			GridColor = Color.Black;
			LivingCellColor = Color.Gray;
			DeadCellColor = Color.White;
			NumberColor = Color.Black;
			HudColor = Color.Red;
			NumberFont = new Font("Arial", 8f);
			HudFont = new Font("Arial", 9f);
			NumberFormat = new StringFormat
			{
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};
			HudFormat = new StringFormat
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Far
			};
			// 2018-11-21 AMR NOTE: Pulled from Settings as Width and Height won't work yet
			_universe = new bool[Properties.Settings.Default.Width, Properties.Settings.Default.Height];
			_scratch = new bool[Width, Height];

			Paint += new PaintEventHandler(_paint);
		}
		/// <summary>
		/// Reset the panel, optionally resizing
		/// </summary>
		/// <param name="width">The new width after reset</param>
		/// <param name="height">The new height after reset</param>
		public void Reset(int? width = null, int? height = null)
		{
			int w = (width.HasValue ? width.Value : Width);
			int h = (height.HasValue ? height.Value : Height);

			_universe = new bool[w, h];
			_scratch = new bool[Width, Height];

			Generation = 0;
			// 2018-11-20 NOTE: Redraw
			this.Invalidate();
		}
		/// <summary>
		/// Get/Set value in cell
		/// </summary>
		/// <param name="x">Column of cell</param>
		/// <param name="y">Height of cell</param>
		/// <returns>Cell</returns>
		public bool this[int x, int y]
		{
			get => _universe[x, y];
			set
			{
				_universe[x, y] = value;
				// 2018-11-20 AMR NOTE: Scratch needs to match Universe except for changes in Next()
				_scratch[x, y] = value;
				// 2018-11-20 AMR NOTE: Redraw
				this.Invalidate();
			}
		}
		/// <summary>
		/// Increment to the next Generation following rules
		/// </summary>
		public void Next()
		{
			// 2018-11-20 AMR NOTE: Apply rules to scratch for each cell
			// 2018-11-20 AMR TODO: Allow custom rules
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int n = Neighbors(x, y);
					if (Alive(x, y) && (n < 2 || n > 3))
						_scratch[x, y] = false;

					if (!Alive(x, y) && n == 3)
						_scratch[x, y] = true;
				}
			}
			// 2018-11-20 AMR NOTE: Swap Universe and Scratch
			(_scratch, _universe) = (_universe, _scratch);
			// 2018-11-20 AMR NOTE: Set scratch to universe now that old scratch isn't needed
			for (int y = 0; y < Height; y++)
				for (int x = 0; x < Width; x++)
					_scratch[x, y] = _universe[x, y];

			++Generation;
			// 2018-11-20 AMR NOTE: Redraw
			this.Invalidate();
		}
		// 2018-11-20 AMR TODO: Wrapped type?
		/// <summary>
		/// Return true if cell at X,Y is alive (Includes wrapping if WrapEdges setting)
		/// </summary>
		/// <param name="x">Column of cell</param>
		/// <param name="y">Height of cell</param>
		/// <returns>Alive/Dead</returns>
		public bool Alive(int x, int y)
		{
			// 2018-11-20 AMR NOTE: Correct X/Y if WrapEdges and out of bounds
			if (WrapEdges)
			{
				if (x < 0)
					x += Width;
				if (x >= Width)
					x -= Width;

				if (y < 0)
					y += Height;
				if (y >= Height)
					y -= Height;
			}
			// 2018-11-20 AMR NOTE: Sanity check for out of bounds
			if (x >= 0 && x < Width && y >= 0 && y < Height)
				return _universe[x, y];
			// 2018-11-20 AMR NOTE: Sanity check failed so out of bounds still (failed wrap or wrap unset)
			return false;
		}
		/// <summary>
		/// Returns the number of "Alive" Adjacent Cells (Includes corners)
		/// </summary>
		/// <param name="x">Column of cell</param>
		/// <param name="y">Height of cell</param>
		/// <returns>Neighbor Count</returns>
		public int Neighbors(int x, int y)
		{
			int ret = 0;
			// 2018-11-20 AMR NOTE: Cells Above
			if (Alive(x - 1, y - 1)) ++ret;
			if (Alive(x, y - 1)) ++ret;
			if (Alive(x + 1, y - 1)) ++ret;
			// 2018-11-20 AMR NOTE: Cells In same row
			if (Alive(x - 1, y)) ++ret;
			if (Alive(x + 1, y)) ++ret;
			// 2018-11-20 AMR NOTE: Cells Below
			if (Alive(x - 1, y + 1)) ++ret;
			if (Alive(x, y + 1)) ++ret;
			if (Alive(x + 1, y + 1)) ++ret;

			return ret;
		}
		/// <summary>
		/// Wrap around the edges to other side instead of considering edge cells 'dead'
		/// </summary>
		[Description("Should Edges wrap or be considered 'dead'"), Category("Behavior"), DefaultValue(true)]
		public bool WrapEdges
		{
			get => _wrap_edges;
			set
			{
				_wrap_edges = value;
				Invalidate();
			}
		}
		/// <summary>
		/// Should the CellPanel draw the Neighbor Count for each non-zero Neighbored cell
		/// </summary>
		[Description("Whether to draw the 'neighbor' count in each cell"), Category("Appearance"), DefaultValue(true)]
		public bool DrawNumbers
		{
			get => _draw_numbers;
			set
			{
				_draw_numbers = value;
				Invalidate();
			}
		}
		/// <summary>
		/// Should the CellPanel draw the Grid Lines
		/// </summary>
		[Description("Whether to draw the grid lines between cells"), Category("Appearance"), DefaultValue(true)]
		public bool DrawLines
		{
			get => _draw_lines;
			set
			{
				_draw_lines = value;
				Invalidate();
			}
		}
		/// <summary>
		/// Should the CellPanel draw the Heads-Up-Display
		/// </summary>
		[Description("Whether to draw the Heads-Up-Display over the cells"), Category("Appearances"), DefaultValue(false)]
		public bool DrawHUD
		{
			get => _draw_hud;
			set
			{
				_draw_hud = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The color of the lines seperating the cells
		/// </summary>
		[Description("Color of the grid lines"), Category("Appearance"), DefaultValue(typeof(Color), "Black")]
		public Color GridColor
		{
			get => _grid_color;
			set
			{
				_grid_color = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The color of the fill for 'living' cells
		/// </summary>
		[Description("Color of the 'living' cells"), Category("Appearance"), DefaultValue(typeof(Color), "Gray")]
		public Color LivingCellColor
		{
			get => _live_cell_color;
			set
			{
				_live_cell_color = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The color of the fill for 'Dead' cells
		/// </summary>
		[Description("Color of the 'dead' cells"), Category("Appearance"), DefaultValue(typeof(Color), "White")]
		public Color DeadCellColor
		{
			get => BackColor;
			set
			{
				BackColor = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The color of the 'neighbor' numbers in cells
		/// </summary>
		[Description("Color of the grid lines"), Category("Appearance"), DefaultValue(typeof(Color), "Black")]
		public Color NumberColor
		{
			get => _number_color;
			set
			{
				_number_color = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The color of the Heads-Up-Display
		/// </summary>
		[Description("Color of the Heads-Up-Display"), Category("Appearances"), DefaultValue(typeof(Color), "Red")]
		public Color HudColor
		{
			get => _hud_color;
			set
			{
				_hud_color = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The font used by 'neighbor' numbers in cells
		/// </summary>
		// 2018-11-21 AMR TODO: Should be built in Font property?
		[Description("Font for numbers of 'neighbor' count"), Category("Appearance")]
		public Font NumberFont
		{
			get => _number_font;
			set
			{
				_number_font = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The font used by the Heads-Up-Display
		/// </summary>
		[Description("Font for Heads-Up-Display"), Category("Appearance")]
		public Font HudFont
		{
			get => _hud_font;
			set
			{
				_hud_font = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The alignment of Cell Numbers
		/// </summary>
		public StringFormat NumberFormat
		{
			get => _number_format;
			set
			{
				_number_format = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The alignment of the Heads-Up-Display
		/// </summary>
		public StringFormat HudFormat
		{
			get => _hud_format;
			set
			{
				_hud_format = value;
				Invalidate();
			}
		}
		/// <summary>
		/// The count of 'living' cells in the whole panel
		/// </summary>
		public int LivingCells
		{
			get
			{
				int ret = 0;
				foreach(bool cell in _universe)
				{
					if(cell)
						++ret;
				}
				return ret;
			}
		}
		/// <summary>
		/// The text for the Heads-Up-Display;
		/// </summary>
		public string Hud
		{
			get => $"Generation: {Generation}\nLiving: {LivingCells}\nSize {Width}x{Height}\nWrapEdges: {WrapEdges}";
		}
		/// <summary>
		/// The number of Generations since last Reset
		/// </summary>
		public int Generation
		{
			get;
			protected set;
		}
		/// <summary>
		/// The number of Columns
		/// </summary>
		public new int Width
		{
			get => _universe.GetLength(0);
		}
		/// <summary>
		/// The number of Rows
		/// </summary>
		public new int Height
		{
			get => _universe.GetLength(1);
		}
		/// <summary>
		/// The width in pixels of a single cell
		/// </summary>
		public int CellWidth
		{
			get => ClientSize.Width / Width;
		}
		/// <summary>
		/// The height in pixels of a single cell
		/// </summary>
		public int CellHeight
		{
			get => ClientSize.Height / Height;
		}
		/// <summary>
		/// Context for drawing operations (internal object only for simplifying paint procedures)
		/// </summary>
		class Context : IDisposable
		{
			public Context(CellPanel parent, Graphics graphics)
			{
				CellBrush = new SolidBrush(parent.LivingCellColor);
				NumberBrush = new SolidBrush(parent.NumberColor);
				HudBrush = new SolidBrush(parent.HudColor);
				GridPen = new Pen(parent.GridColor);

				Graphics = graphics;
			}
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}
			protected virtual void Dispose(bool disposing)
			{
				if (disposing)
				{
					if(CellBrush != null) CellBrush.Dispose();
					if (NumberBrush != null) NumberBrush.Dispose();
					if (HudBrush != null) HudBrush.Dispose();
					if (GridPen != null) GridPen.Dispose();
				}
			}
			public Brush CellBrush
			{
				get;
				private set;
			}
			public Brush NumberBrush
			{
				get;
				private set;
			}
			public Brush HudBrush
			{
				get;
				private set;
			}
			public Pen GridPen
			{
				get;
				private set;
			}
			public Graphics Graphics
			{
				get;
				private set;
			}
		}
		/// <summary>
		/// OnPaint event, Draws the component
		/// </summary>
		/// <param name="e">Unused</param>
		/// <param name="a">The PaintEventArgs provided by OnPaint event</param>
		void _paint(object e, PaintEventArgs a)
		{
			using (Context ctx = new Context(this, a.Graphics))
			{
				for (int y = 0; y < Height; y++)
				{
					for (int x = 0; x < Width; x++)
					{
						// 2018-11-20 AMR NOTE: Draw the Cell (Fill on Alive, Write Numbers on Neighbors)
						_paint_cell(ctx, x, y);
						// 2018-11-19 AMR NOTE: Draw the grid on top
						if(DrawLines)
							_paint_grid(ctx, x, y);
						// 2018-12-09 AMR NOTE: Draw the HUD
						if (DrawHUD)
							_paint_hud(ctx);
					}
				}
			}
		}
		/// <summary>
		/// Paint the cells at X and Y using ctx
		/// </summary>
		/// <param name="ctx">The 'context' (includes graphics context and drawing objects used for this objects paint)</param>
		/// <param name="x">'X' Position of cell to draw (how many cells from the left)</param>
		/// <param name="y">'Y' Position of cell to draw (how many cells from the top)</param>
		void _paint_cell(Context ctx, int x, int y)
		{
			Rectangle rect = new Rectangle(x * CellWidth, y * CellHeight, CellWidth, CellHeight);
			int neighbors = Neighbors(x, y);

			if (Alive(x, y))
				_fill_cell(ctx, rect);
			if (DrawNumbers && neighbors != 0)
				_paint_numbers(ctx, rect, neighbors);
		}
		/// <summary>
		/// Paint the grid around the cell at X and Y using ctx
		/// </summary>
		/// <param name="ctx">The 'context' (includes graphics context and drawing objects used for this objects paint)</param>
		/// <param name="x">'X' Position of cell to draw (how many cells from the left)</param>
		/// <param name="y">'Y' Position of cell to draw (how many cells from the top)</param>
		void _paint_grid(Context ctx, int x, int y)
		{
			ctx.Graphics.DrawRectangle(ctx.GridPen, x * CellWidth, y * CellHeight, CellWidth, CellHeight);
		}
		/// <summary>
		/// Fill the cell in
		/// </summary>
		/// <param name="ctx">The 'context' (includes graphics context and drawing objects used for this objects paint)</param>
		/// <param name="rect">The rectangle around the cell</param>
		void _fill_cell(Context ctx, Rectangle rect)
		{
			ctx.Graphics.FillRectangle(ctx.CellBrush, rect);
		}
		/// <summary>
		/// Draw the neighbor numbers on the cell
		/// </summary>
		/// <param name="ctx">The 'context' (includes graphics context and drawing objects used for this objects paint)</param>
		/// <param name="rect">The rectangle around the cell</param>
		/// <param name="neighbors">The count of neighbors to draw</param>
		void _paint_numbers(Context ctx, Rectangle rect, int neighbors)
		{
			ctx.Graphics.DrawString(neighbors.ToString(), NumberFont, ctx.NumberBrush, rect, NumberFormat);
		}
		/// <summary>
		/// The Heads-Up-Display for cell panel
		/// </summary>
		/// <param name="ctx">The 'context' (includes graphics context and drawing objects used for this objects paint)</param>
		void _paint_hud(Context ctx)
		{
			Rectangle rect = new Rectangle(Top, Left, ((Control)this).Width, ((Control)this).Height);
			ctx.Graphics.DrawString(Hud, HudFont, ctx.HudBrush, rect, HudFormat);
		}
		bool[,] _universe;
		bool[,] _scratch;
		bool _wrap_edges;
		bool _draw_numbers;
		bool _draw_lines;
		bool _draw_hud;
		StringFormat _number_format;
		StringFormat _hud_format;
		Color _grid_color;
		Color _live_cell_color;
		Color _number_color;
		Color _hud_color;
		Font _number_font;
		Font _hud_font;
	}
}
