﻿using System;
using System.Windows.Forms;

namespace GameOfLife
{
	class GraphicsPanel : Panel
	{
		public GraphicsPanel()
		{
			this.DoubleBuffered = true;
			this.SetStyle(ControlStyles.ResizeRedraw, true);
		}
	}
}
