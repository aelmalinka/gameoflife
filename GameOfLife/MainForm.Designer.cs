﻿namespace GameOfLife
{
    partial class MainForm
{
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing)
	{
		if (disposing && (components != null))
		{
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
	{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			System.Drawing.StringFormat stringFormat3 = new System.Drawing.StringFormat();
			System.Drawing.StringFormat stringFormat4 = new System.Drawing.StringFormat();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.randomizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.numbersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.linesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.hudToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.options = new System.Windows.Forms.ToolStripMenuItem();
			this.gridColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.gridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cellToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.livingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.numberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fontsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.numbersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.reloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.newToolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.openToolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.saveToolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStart = new System.Windows.Forms.ToolStripButton();
			this.toolStop = new System.Windows.Forms.ToolStripButton();
			this.toolNext = new System.Windows.Forms.ToolStripButton();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.lblGenerations = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblLiving = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblFile = new System.Windows.Forms.ToolStripStatusLabel();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.mnuRight = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.numbersToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.linesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.hudToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.randomizeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.cells = new GameOfLife.CellPanel();
			this.wrapEdgesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.wrapEdgesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.mnuRight.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolsToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(741, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator4,
            this.importToolStripMenuItem,
            this.randomizeToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.newToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.newToolStripMenuItem.Text = "&New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripButton_Click);
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(171, 6);
			// 
			// importToolStripMenuItem
			// 
			this.importToolStripMenuItem.Name = "importToolStripMenuItem";
			this.importToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
			this.importToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.importToolStripMenuItem.Text = "&Import";
			this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
			// 
			// randomizeToolStripMenuItem
			// 
			this.randomizeToolStripMenuItem.Name = "randomizeToolStripMenuItem";
			this.randomizeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
			this.randomizeToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.randomizeToolStripMenuItem.Text = "&Randomize";
			this.randomizeToolStripMenuItem.Click += new System.EventHandler(this.randomizeToolStripMenuItem_Click);
			// 
			// toolStripSeparator
			// 
			this.toolStripSeparator.Name = "toolStripSeparator";
			this.toolStripSeparator.Size = new System.Drawing.Size(171, 6);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.saveAsToolStripMenuItem.Text = "Save &As";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(171, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			// 
			// viewToolStripMenuItem
			// 
			this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numbersToolStripMenuItem1,
            this.linesToolStripMenuItem,
            this.hudToolStripMenuItem,
            this.wrapEdgesToolStripMenuItem1});
			this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
			this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.viewToolStripMenuItem.Text = "&View";
			// 
			// numbersToolStripMenuItem1
			// 
			this.numbersToolStripMenuItem1.Name = "numbersToolStripMenuItem1";
			this.numbersToolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
			this.numbersToolStripMenuItem1.Text = "&Numbers";
			this.numbersToolStripMenuItem1.Click += new System.EventHandler(this.numbersToolStripMenuItem1_Click);
			// 
			// linesToolStripMenuItem
			// 
			this.linesToolStripMenuItem.Name = "linesToolStripMenuItem";
			this.linesToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.linesToolStripMenuItem.Text = "&Lines";
			this.linesToolStripMenuItem.Click += new System.EventHandler(this.linesToolStripMenuItem_Click);
			// 
			// hudToolStripMenuItem
			// 
			this.hudToolStripMenuItem.Name = "hudToolStripMenuItem";
			this.hudToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.hudToolStripMenuItem.Text = "&Hud";
			this.hudToolStripMenuItem.Click += new System.EventHandler(this.hudToolStripMenuItem_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.options,
            this.gridColorToolStripMenuItem,
            this.fontsToolStripMenuItem,
            this.toolStripSeparator1,
            this.reloadToolStripMenuItem,
            this.resetToolStripMenuItem});
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
			this.toolsToolStripMenuItem.Text = "&Tools";
			// 
			// options
			// 
			this.options.Name = "options";
			this.options.Size = new System.Drawing.Size(116, 22);
			this.options.Text = "&Options";
			this.options.Click += new System.EventHandler(this.options_Click);
			// 
			// gridColorToolStripMenuItem
			// 
			this.gridColorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gridToolStripMenuItem,
            this.cellToolStripMenuItem,
            this.numberToolStripMenuItem});
			this.gridColorToolStripMenuItem.Name = "gridColorToolStripMenuItem";
			this.gridColorToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.gridColorToolStripMenuItem.Text = "&Colors";
			// 
			// gridToolStripMenuItem
			// 
			this.gridToolStripMenuItem.Name = "gridToolStripMenuItem";
			this.gridToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
			this.gridToolStripMenuItem.Text = "&Grid";
			this.gridToolStripMenuItem.Click += new System.EventHandler(this.gridToolStripMenuItem_Click);
			// 
			// cellToolStripMenuItem
			// 
			this.cellToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.livingToolStripMenuItem,
            this.deadToolStripMenuItem});
			this.cellToolStripMenuItem.Name = "cellToolStripMenuItem";
			this.cellToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
			this.cellToolStripMenuItem.Text = "&Cells";
			// 
			// livingToolStripMenuItem
			// 
			this.livingToolStripMenuItem.Name = "livingToolStripMenuItem";
			this.livingToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
			this.livingToolStripMenuItem.Text = "&Living";
			this.livingToolStripMenuItem.Click += new System.EventHandler(this.livingToolStripMenuItem_Click);
			// 
			// deadToolStripMenuItem
			// 
			this.deadToolStripMenuItem.Name = "deadToolStripMenuItem";
			this.deadToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
			this.deadToolStripMenuItem.Text = "&Dead";
			this.deadToolStripMenuItem.Click += new System.EventHandler(this.deadToolStripMenuItem_Click);
			// 
			// numberToolStripMenuItem
			// 
			this.numberToolStripMenuItem.Name = "numberToolStripMenuItem";
			this.numberToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
			this.numberToolStripMenuItem.Text = "&Number";
			this.numberToolStripMenuItem.Click += new System.EventHandler(this.numberToolStripMenuItem_Click);
			// 
			// fontsToolStripMenuItem
			// 
			this.fontsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numbersToolStripMenuItem});
			this.fontsToolStripMenuItem.Name = "fontsToolStripMenuItem";
			this.fontsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.fontsToolStripMenuItem.Text = "Fonts";
			// 
			// numbersToolStripMenuItem
			// 
			this.numbersToolStripMenuItem.Name = "numbersToolStripMenuItem";
			this.numbersToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.numbersToolStripMenuItem.Text = "Numbers";
			this.numbersToolStripMenuItem.Click += new System.EventHandler(this.numbersToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(113, 6);
			// 
			// reloadToolStripMenuItem
			// 
			this.reloadToolStripMenuItem.Name = "reloadToolStripMenuItem";
			this.reloadToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.reloadToolStripMenuItem.Text = "Re&load";
			this.reloadToolStripMenuItem.Click += new System.EventHandler(this.reloadToolStripMenuItem_Click);
			// 
			// resetToolStripMenuItem
			// 
			this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
			this.resetToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.resetToolStripMenuItem.Text = "&Reset";
			this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton1,
            this.openToolStripButton1,
            this.saveToolStripButton1,
            this.toolStripSeparator3,
            this.toolStart,
            this.toolStop,
            this.toolNext});
			this.toolStrip1.Location = new System.Drawing.Point(0, 24);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(741, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// newToolStripButton1
			// 
			this.newToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.newToolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton1.Image")));
			this.newToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.newToolStripButton1.Name = "newToolStripButton1";
			this.newToolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.newToolStripButton1.Text = "&New";
			this.newToolStripButton1.Click += new System.EventHandler(this.newToolStripButton_Click);
			// 
			// openToolStripButton1
			// 
			this.openToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.openToolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton1.Image")));
			this.openToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.openToolStripButton1.Name = "openToolStripButton1";
			this.openToolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.openToolStripButton1.Text = "&Open";
			this.openToolStripButton1.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// saveToolStripButton1
			// 
			this.saveToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.saveToolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton1.Image")));
			this.saveToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.saveToolStripButton1.Name = "saveToolStripButton1";
			this.saveToolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.saveToolStripButton1.Text = "&Save";
			this.saveToolStripButton1.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStart
			// 
			this.toolStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStart.Image = ((System.Drawing.Image)(resources.GetObject("toolStart.Image")));
			this.toolStart.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStart.Name = "toolStart";
			this.toolStart.Size = new System.Drawing.Size(23, 22);
			this.toolStart.Text = "S&tart";
			this.toolStart.ToolTipText = "Start Run";
			this.toolStart.Click += new System.EventHandler(this.toolStart_Click);
			// 
			// toolStop
			// 
			this.toolStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStop.Image = global::GameOfLife.Properties.Resources.Stop;
			this.toolStop.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStop.Name = "toolStop";
			this.toolStop.Size = new System.Drawing.Size(23, 22);
			this.toolStop.Text = "St&op";
			this.toolStop.Click += new System.EventHandler(this.toolStop_Click);
			// 
			// toolNext
			// 
			this.toolNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNext.Image = global::GameOfLife.Properties.Resources.Next;
			this.toolNext.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNext.Name = "toolNext";
			this.toolNext.Size = new System.Drawing.Size(23, 22);
			this.toolNext.Text = "Ne&xt";
			this.toolNext.Click += new System.EventHandler(this.increment);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblGenerations,
            this.lblLiving,
            this.lblFile});
			this.statusStrip1.Location = new System.Drawing.Point(0, 549);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(741, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// lblGenerations
			// 
			this.lblGenerations.Name = "lblGenerations";
			this.lblGenerations.Size = new System.Drawing.Size(77, 17);
			this.lblGenerations.Text = "Generation: 0";
			// 
			// lblLiving
			// 
			this.lblLiving.Name = "lblLiving";
			this.lblLiving.Size = new System.Drawing.Size(51, 17);
			this.lblLiving.Text = "Living: 0";
			// 
			// lblFile
			// 
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(0, 17);
			// 
			// timer
			// 
			this.timer.Interval = 50;
			this.timer.Tick += new System.EventHandler(this.increment);
			// 
			// mnuRight
			// 
			this.mnuRight.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem1,
            this.randomizeToolStripMenuItem1,
            this.toolStripSeparator5,
            this.numbersToolStripMenuItem2,
            this.linesToolStripMenuItem1,
            this.hudToolStripMenuItem1,
            this.wrapEdgesToolStripMenuItem});
			this.mnuRight.Name = "mnuRight";
			this.mnuRight.Size = new System.Drawing.Size(137, 142);
			// 
			// numbersToolStripMenuItem2
			// 
			this.numbersToolStripMenuItem2.Name = "numbersToolStripMenuItem2";
			this.numbersToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
			this.numbersToolStripMenuItem2.Text = "&Numbers";
			this.numbersToolStripMenuItem2.Click += new System.EventHandler(this.numbersToolStripMenuItem1_Click);
			// 
			// linesToolStripMenuItem1
			// 
			this.linesToolStripMenuItem1.Name = "linesToolStripMenuItem1";
			this.linesToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
			this.linesToolStripMenuItem1.Text = "&Lines";
			this.linesToolStripMenuItem1.Click += new System.EventHandler(this.linesToolStripMenuItem_Click);
			// 
			// hudToolStripMenuItem1
			// 
			this.hudToolStripMenuItem1.Name = "hudToolStripMenuItem1";
			this.hudToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
			this.hudToolStripMenuItem1.Text = "&Hud";
			this.hudToolStripMenuItem1.Click += new System.EventHandler(this.hudToolStripMenuItem_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(177, 6);
			// 
			// newToolStripMenuItem1
			// 
			this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
			this.newToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
			this.newToolStripMenuItem1.Text = "&New";
			this.newToolStripMenuItem1.Click += new System.EventHandler(this.newToolStripButton_Click);
			// 
			// randomizeToolStripMenuItem1
			// 
			this.randomizeToolStripMenuItem1.Name = "randomizeToolStripMenuItem1";
			this.randomizeToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
			this.randomizeToolStripMenuItem1.Text = "&Randomize";
			this.randomizeToolStripMenuItem1.Click += new System.EventHandler(this.randomizeToolStripMenuItem_Click);
			// 
			// cells
			// 
			this.cells.BackColor = System.Drawing.Color.White;
			this.cells.ContextMenuStrip = this.mnuRight;
			this.cells.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cells.HudFont = new System.Drawing.Font("Arial", 9F);
			stringFormat3.Alignment = System.Drawing.StringAlignment.Near;
			stringFormat3.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.None;
			stringFormat3.LineAlignment = System.Drawing.StringAlignment.Far;
			stringFormat3.Trimming = System.Drawing.StringTrimming.Character;
			this.cells.HudFormat = stringFormat3;
			this.cells.Location = new System.Drawing.Point(0, 49);
			this.cells.Name = "cells";
			this.cells.NumberFont = new System.Drawing.Font("Arial", 8F);
			stringFormat4.Alignment = System.Drawing.StringAlignment.Center;
			stringFormat4.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.None;
			stringFormat4.LineAlignment = System.Drawing.StringAlignment.Center;
			stringFormat4.Trimming = System.Drawing.StringTrimming.Character;
			this.cells.NumberFormat = stringFormat4;
			this.cells.Size = new System.Drawing.Size(741, 500);
			this.cells.TabIndex = 3;
			this.cells.MouseUp += new System.Windows.Forms.MouseEventHandler(this.cells_MouseUp);
			// 
			// wrapEdgesToolStripMenuItem
			// 
			this.wrapEdgesToolStripMenuItem.Name = "wrapEdgesToolStripMenuItem";
			this.wrapEdgesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.wrapEdgesToolStripMenuItem.Text = "Wrap &Edges";
			this.wrapEdgesToolStripMenuItem.Click += new System.EventHandler(this.wrapEdgesToolStripMenuItem_Click);
			// 
			// wrapEdgesToolStripMenuItem1
			// 
			this.wrapEdgesToolStripMenuItem1.Name = "wrapEdgesToolStripMenuItem1";
			this.wrapEdgesToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
			this.wrapEdgesToolStripMenuItem1.Text = "Wrap &Edges";
			this.wrapEdgesToolStripMenuItem1.Click += new System.EventHandler(this.wrapEdgesToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(741, 571);
			this.Controls.Add(this.cells);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = typeof(GameOfLife.Properties.Resources).Name;
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.mnuRight.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

	}

	#endregion

	private System.Windows.Forms.MenuStrip menuStrip1;
	private System.Windows.Forms.ToolStrip toolStrip1;
	private System.Windows.Forms.StatusStrip statusStrip1;
	private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
	private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
	private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
	private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
	private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
	private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
	private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
	private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
	private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
	private System.Windows.Forms.ToolStripMenuItem options;
	private System.Windows.Forms.ToolStripStatusLabel lblGenerations;
		private System.Windows.Forms.Timer timer;
		private System.Windows.Forms.ToolStripButton newToolStripButton1;
		private System.Windows.Forms.ToolStripButton openToolStripButton1;
		private System.Windows.Forms.ToolStripButton saveToolStripButton1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton toolStart;
		private System.Windows.Forms.ToolStripButton toolStop;
		private System.Windows.Forms.ToolStripButton toolNext;
		private System.Windows.Forms.ToolStripMenuItem gridColorToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem gridToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cellToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem numberToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fontsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem numbersToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem livingToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deadToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem reloadToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem randomizeToolStripMenuItem;
		private System.Windows.Forms.ToolStripStatusLabel lblFile;
		private System.Windows.Forms.ToolStripStatusLabel lblLiving;
		private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem numbersToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem linesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem hudToolStripMenuItem;
		private CellPanel cells;
		private System.Windows.Forms.ContextMenuStrip mnuRight;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem randomizeToolStripMenuItem1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripMenuItem numbersToolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem linesToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem hudToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem wrapEdgesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem wrapEdgesToolStripMenuItem1;
	}
}