﻿using System;
using System.Windows.Forms;

namespace GameOfLife
{
	public partial class MainForm : Form
	{
		/// <summary>
		/// Construct the MainForm
		/// </summary>
		public MainForm()
		{
			InitializeComponent();
			Settings();
			// 2018-11-21 AMR NOTE: Initialize seed to something
			Seed = (int)DateTime.Now.Ticks;
		}
		/// <summary>
		/// Set form values from Settings
		/// </summary>
		public void Settings()
		{
			timer.Interval = Properties.Settings.Default.UpdateInterval;
			wrapEdgesToolStripMenuItem1.Checked = wrapEdgesToolStripMenuItem.Checked = cells.WrapEdges = Properties.Settings.Default.WrapEdges;
			numbersToolStripMenuItem2.Checked = numbersToolStripMenuItem1.Checked = cells.DrawNumbers = Properties.Settings.Default.DrawNumbers;
			linesToolStripMenuItem1.Checked = linesToolStripMenuItem.Checked = cells.DrawLines = Properties.Settings.Default.DrawLines;
			hudToolStripMenuItem1.Checked = hudToolStripMenuItem.Checked = cells.DrawHUD = Properties.Settings.Default.DrawHUD;
			cells.LivingCellColor = Properties.Settings.Default.LivingCellColor;
			cells.DeadCellColor = Properties.Settings.Default.DeadCellColor;
			cells.GridColor = Properties.Settings.Default.GridColor;
			cells.NumberColor = Properties.Settings.Default.NumberColor;
			cells.NumberFont = Properties.Settings.Default.NumberFont;
		}
		/// <summary>
		/// The Seed to be used for Random later
		/// </summary>
		public int Seed
		{
			get;
			private set;
		}
		/// <summary>
		/// Called when form is closed, saves properties
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void MainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			Properties.Settings.Default.Save();
		}
		/// <summary>
		/// Used to Spawn/Kill the particular Cell
		/// 
		/// Note: MouseUp is used to avoid parseing of DoubleClick Events (All clicks no matter how quickly will toggle the cell)
		/// </summary>
		/// <param name="e">Unused</param>
		/// <param name="a">Mouse Location/Buttons provided by MouseUp Event</param>
		void cells_MouseUp(object e, MouseEventArgs a)
		{
			if (a.Button == MouseButtons.Left)
			{
				int x = a.X / cells.CellWidth;
				int y = a.Y / cells.CellHeight;

				cells[x, y] = !cells[x, y];
				lblLiving.Text = $"Living: {cells.LivingCells}";
			}
		}
		/// <summary>
		/// Reset the CellPanel
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void newToolStripButton_Click(object sender, EventArgs e)
		{
			file = null;
			timer.Enabled = false;
			cells.Reset();
			lblGenerations.Text = $"Generation: {cells.Generation}";
		}
		/// <summary>
		/// Opens cells file
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			timer.Enabled = false;

			using (OpenFileDialog form = new OpenFileDialog())
			{
				form.Filter = "All Files|*.*|Cell Files|*.cells";
				form.FilterIndex = 2;

				if (form.ShowDialog() == DialogResult.OK)
				{
					file = form.FileName;

					using (UniverseReader reader = new UniverseReader(file))
					{
						reader.Read(cells);
						lblFile.Text = $"File: {file}";
					}
				}
			}
		}
		/// <summary>
		/// Import cell file into current universe
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void importToolStripMenuItem_Click(object sender, EventArgs e)
		{
			timer.Enabled = false;

			using (OpenFileDialog form = new OpenFileDialog())
			{
				form.Filter = "All Files|*.*|Cell Files|*.cells";
				form.FilterIndex = 2;

				if (form.ShowDialog() == DialogResult.OK)
				{
					using (UniverseReader reader = new UniverseReader(form.FileName))
					{
						reader.Read(cells, false);
						lblFile.Text = $"File: {file}";
					}
				}
			}
		}
		/// <summary>
		/// Save current file
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			// 2018-12-07 AMR NOTE: Unset Filename
			if (file == null)
			{
				using (SaveFileDialog form = new SaveFileDialog())
				{
					form.Filter = "All Files|*.*|Cell Files|*.cells";
					form.FilterIndex = 2;
					form.DefaultExt = "cells";

					if (form.ShowDialog() == DialogResult.OK)
					{
						file = form.FileName;
						lblFile.Text = $"File: {file}";
					}
				}
			}
			// 2018-12-07 AMR NOTE: if file is still null the user canceled
			if (file != null)
			{
				using (UniverseWriter writer = new UniverseWriter(file))
				{
					writer.Write(cells);
				}
			}
		}
		/// <summary>
		/// Save as a different file
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (SaveFileDialog form = new SaveFileDialog())
			{
				form.FileName = file;
				form.Filter = "All Files|*.*|Cell Files|*.cells";
				form.FilterIndex = 2;
				form.DefaultExt = "cells";

				if (form.ShowDialog() == DialogResult.OK)
				{
					file = form.FileName;

					using (UniverseWriter writer = new UniverseWriter(file))
					{
						writer.Write(cells);
						lblFile.Text = $"File: {file}";
					}
				}
			}
		}
		/// <summary>
		/// Increment the generation of the CellPanel
		/// 
		/// Note: Both Next::Click and timer::Tick use this event
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void increment(object sender, EventArgs e)
		{
			cells.Next();
			lblGenerations.Text = $"Generation: {cells.Generation}";
			lblLiving.Text = $"Living: {cells.LivingCells}";
		}
		/// <summary>
		/// Start timer
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void toolStart_Click(object sender, EventArgs e)
		{
			timer.Enabled = true;
		}
		/// <summary>
		/// Stop timer
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void toolStop_Click(object sender, EventArgs e)
		{
			timer.Enabled = false;
		}
		/// <summary>
		/// Reset the universe to random values based on dialogue
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void randomizeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (RandomForm form = new RandomForm())
			{
				form.Living = 50;
				form.Seed = Seed;

				if(form.ShowDialog() == DialogResult.OK)
				{
					cells.Reset();

					Seed = (int)form.Seed;
					Random r = new Random(Seed);

					for (int x = 0; x < cells.Width; x++)
					{
						for (int y = 0; y < cells.Height; y++)
						{
							if (r.Next(100) < form.Living)
								cells[x, y] = true;
						}
					}
				}
			}

			lblLiving.Text = $"Living: {cells.LivingCells}";
		}
		/// <summary>
		/// Option to Enable/Disable drawing of grid lines between cells
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void linesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			linesToolStripMenuItem1.Checked = linesToolStripMenuItem.Checked = !linesToolStripMenuItem.Checked;
			cells.DrawLines = Properties.Settings.Default.DrawLines = linesToolStripMenuItem.Checked;
		}
		/// <summary>
		/// Option to Enable/Disable drawing of the 'neighbor' count numbers
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void numbersToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			numbersToolStripMenuItem2.Checked = numbersToolStripMenuItem1.Checked = !numbersToolStripMenuItem1.Checked;
			cells.DrawNumbers = Properties.Settings.Default.DrawLines = numbersToolStripMenuItem1.Checked;
		}
		/// <summary>
		/// Option to Enable/Disable drawing of the Heads-Up-Display
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void hudToolStripMenuItem_Click(object sender, EventArgs e)
		{
			hudToolStripMenuItem1.Checked = hudToolStripMenuItem.Checked = !hudToolStripMenuItem.Checked;
			cells.DrawHUD = Properties.Settings.Default.DrawHUD = hudToolStripMenuItem.Checked;
		}
		/// <summary>
		/// Option to Enable/Disable Edge Wrap
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void wrapEdgesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			wrapEdgesToolStripMenuItem1.Checked = wrapEdgesToolStripMenuItem.Checked = !wrapEdgesToolStripMenuItem.Checked;
			cells.WrapEdges = Properties.Settings.Default.WrapEdges = wrapEdgesToolStripMenuItem.Checked;
		}
		/// <summary>
		/// Reset the settings to the currently written values
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void reloadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Properties.Settings.Default.Reload();
			Settings();
		}
		/// <summary>
		/// Reset the settings to the default values
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void resetToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Properties.Settings.Default.Reset();
			Settings();
		}
		/// <summary>
		/// Display the Options Dialogue
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void options_Click(object sender, EventArgs e)
		{
			using (OptionsForm form = new OptionsForm())
			{
				form.Width = cells.Width;
				form.Height = cells.Height;
				form.Seed = Seed;
				form.Speed = timer.Interval;
				form.ShowNumbers = cells.DrawNumbers;
				form.ShowGrid = cells.DrawLines;
				form.ShowHUD = cells.DrawHUD;
				form.WrapEdges = cells.WrapEdges;

				if (form.ShowDialog() == DialogResult.OK)
				{
					if (form.Width != cells.Width || form.Height != cells.Height)
					{
						timer.Enabled = false;
						cells.Reset((int)form.Width, (int)form.Height);
					}

					Seed = (int)form.Seed;
					Properties.Settings.Default.UpdateInterval = timer.Interval = (int)form.Speed;
					Properties.Settings.Default.DrawNumbers = numbersToolStripMenuItem2.Checked = numbersToolStripMenuItem1.Checked = cells.DrawNumbers = form.ShowNumbers;
					Properties.Settings.Default.DrawLines = linesToolStripMenuItem1.Checked = linesToolStripMenuItem.Checked = cells.DrawLines = form.ShowGrid;
					Properties.Settings.Default.DrawHUD = hudToolStripMenuItem1.Checked = hudToolStripMenuItem.Checked = cells.DrawHUD = form.ShowHUD;
					Properties.Settings.Default.WrapEdges = wrapEdgesToolStripMenuItem1.Checked = wrapEdgesToolStripMenuItem.Checked = cells.WrapEdges = form.WrapEdges;
				}
			}
		}
		/// <summary>
		/// Display color picker for Grid Lines
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void gridToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (ColorDialog form = new ColorDialog())
			{
				form.Color = cells.GridColor;

				if (form.ShowDialog() == DialogResult.OK)
					Properties.Settings.Default.GridColor = cells.GridColor = form.Color;
			}
		}
		/// <summary>
		/// Display color picker for "Neighbor" Numbers
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void numberToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (ColorDialog form = new ColorDialog())
			{
				form.Color = cells.NumberColor;

				if (form.ShowDialog() == DialogResult.OK)
					Properties.Settings.Default.NumberColor = cells.NumberColor = form.Color;
			}
		}
		/// <summary>
		/// Display color picker for Living Cells
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void livingToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (ColorDialog form = new ColorDialog())
			{
				form.Color = cells.LivingCellColor;

				if (form.ShowDialog() == DialogResult.OK)
					Properties.Settings.Default.LivingCellColor = cells.LivingCellColor = form.Color;
			}
		}
		/// <summary>
		/// Display color picker for Dead Cells
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void deadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (ColorDialog form = new ColorDialog())
			{
				form.Color = cells.DeadCellColor;

				if (form.ShowDialog() == DialogResult.OK)
					Properties.Settings.Default.DeadCellColor = cells.DeadCellColor = form.Color;
			}
		}
		/// <summary>
		/// Font of "Neighbor" Numbers
		/// </summary>
		/// <param name="sender">Unused</param>
		/// <param name="e">Unused</param>
		void numbersToolStripMenuItem_Click(object sender, EventArgs e)
		{
			using (FontDialog form = new FontDialog())
			{
				form.Font = cells.NumberFont;

				if(form.ShowDialog() == DialogResult.OK)
					Properties.Settings.Default.NumberFont = cells.NumberFont = form.Font;
			}
		}
		/// <summary>
		/// Main Entry point of Application
		/// </summary>
		[STAThread]
		public static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
		string file = null;
	}
}
