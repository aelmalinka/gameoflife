﻿namespace GameOfLife
{
	partial class OptionsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.txtSpeed = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.txtSeed = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtWidth = new System.Windows.Forms.NumericUpDown();
			this.txtHeight = new System.Windows.Forms.NumericUpDown();
			this.chkWrap = new System.Windows.Forms.CheckBox();
			this.chkNumbers = new System.Windows.Forms.CheckBox();
			this.cancel = new System.Windows.Forms.Button();
			this.ok = new System.Windows.Forms.Button();
			this.chkGrid = new System.Windows.Forms.CheckBox();
			this.chkHud = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtSpeed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.chkHud);
			this.panel1.Controls.Add(this.chkGrid);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.txtSpeed);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.txtSeed);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.txtWidth);
			this.panel1.Controls.Add(this.txtHeight);
			this.panel1.Controls.Add(this.chkWrap);
			this.panel1.Controls.Add(this.chkNumbers);
			this.panel1.Controls.Add(this.cancel);
			this.panel1.Controls.Add(this.ok);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(244, 236);
			this.panel1.TabIndex = 0;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 87);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(38, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Speed";
			// 
			// txtSpeed
			// 
			this.txtSpeed.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.txtSpeed.Location = new System.Drawing.Point(85, 85);
			this.txtSpeed.Maximum = new decimal(new int[] {
            120000,
            0,
            0,
            0});
			this.txtSpeed.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.txtSpeed.Name = "txtSpeed";
			this.txtSpeed.Size = new System.Drawing.Size(120, 20);
			this.txtSpeed.TabIndex = 9;
			this.txtSpeed.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 61);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(32, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Seed";
			// 
			// txtSeed
			// 
			this.txtSeed.Location = new System.Drawing.Point(85, 59);
			this.txtSeed.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
			this.txtSeed.Minimum = new decimal(new int[] {
            -2147483648,
            0,
            0,
            -2147483648});
			this.txtSeed.Name = "txtSeed";
			this.txtSeed.Size = new System.Drawing.Size(120, 20);
			this.txtSeed.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 35);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(35, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Width";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Height";
			// 
			// txtWidth
			// 
			this.txtWidth.Location = new System.Drawing.Point(85, 33);
			this.txtWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.txtWidth.Name = "txtWidth";
			this.txtWidth.Size = new System.Drawing.Size(120, 20);
			this.txtWidth.TabIndex = 1;
			// 
			// txtHeight
			// 
			this.txtHeight.Location = new System.Drawing.Point(85, 7);
			this.txtHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.txtHeight.Name = "txtHeight";
			this.txtHeight.Size = new System.Drawing.Size(120, 20);
			this.txtHeight.TabIndex = 0;
			// 
			// chkWrap
			// 
			this.chkWrap.AutoSize = true;
			this.chkWrap.Location = new System.Drawing.Point(12, 180);
			this.chkWrap.Name = "chkWrap";
			this.chkWrap.Size = new System.Drawing.Size(85, 17);
			this.chkWrap.TabIndex = 4;
			this.chkWrap.Text = "Wrap Edges";
			this.chkWrap.UseVisualStyleBackColor = true;
			// 
			// chkNumbers
			// 
			this.chkNumbers.AutoSize = true;
			this.chkNumbers.Location = new System.Drawing.Point(12, 111);
			this.chkNumbers.Name = "chkNumbers";
			this.chkNumbers.Size = new System.Drawing.Size(137, 17);
			this.chkNumbers.TabIndex = 3;
			this.chkNumbers.Text = "Display Neighbor Count";
			this.chkNumbers.UseVisualStyleBackColor = true;
			// 
			// cancel
			// 
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.Location = new System.Drawing.Point(157, 203);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 7;
			this.cancel.Text = "&Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			// 
			// ok
			// 
			this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ok.Location = new System.Drawing.Point(12, 203);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(75, 23);
			this.ok.TabIndex = 5;
			this.ok.Text = "&Ok";
			this.ok.UseVisualStyleBackColor = true;
			// 
			// chkGrid
			// 
			this.chkGrid.AutoSize = true;
			this.chkGrid.Location = new System.Drawing.Point(12, 134);
			this.chkGrid.Name = "chkGrid";
			this.chkGrid.Size = new System.Drawing.Size(82, 17);
			this.chkGrid.TabIndex = 11;
			this.chkGrid.Text = "Display Grid";
			this.chkGrid.UseVisualStyleBackColor = true;
			// 
			// chkHud
			// 
			this.chkHud.AutoSize = true;
			this.chkHud.Location = new System.Drawing.Point(12, 157);
			this.chkHud.Name = "chkHud";
			this.chkHud.Size = new System.Drawing.Size(148, 17);
			this.chkHud.TabIndex = 12;
			this.chkHud.Text = "Display Heads Up Display";
			this.chkHud.UseVisualStyleBackColor = true;
			// 
			// OptionsForm
			// 
			this.AcceptButton = this.ok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(244, 236);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "OptionsForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Options";
			this.TopMost = true;
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtSpeed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSeed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.CheckBox chkWrap;
		private System.Windows.Forms.CheckBox chkNumbers;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown txtWidth;
		private System.Windows.Forms.NumericUpDown txtHeight;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown txtSeed;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown txtSpeed;
		private System.Windows.Forms.CheckBox chkHud;
		private System.Windows.Forms.CheckBox chkGrid;
	}
}