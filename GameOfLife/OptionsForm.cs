﻿using System;
using System.Windows.Forms;

namespace GameOfLife
{
	public partial class OptionsForm : Form
	{
		public OptionsForm()
		{
			InitializeComponent();
		}
		/// <summary>
		/// The number of cells tall
		/// </summary>
		public new decimal Height
		{
			get => txtHeight.Value;
			set => txtHeight.Value = value;
		}
		/// <summary>
		/// The number of cells wide
		/// </summary>
		public new decimal Width
		{
			get => txtWidth.Value;
			set => txtWidth.Value = value;
		}
		/// <summary>
		/// The seed for psuedo-random number generation
		/// </summary>
		public decimal Seed
		{
			get => txtSeed.Value;
			set => txtSeed.Value = value;
		}
		/// <summary>
		/// The time between updates in milliseconds
		/// </summary>
		public decimal Speed
		{
			get => txtSpeed.Value;
			set => txtSpeed.Value = value;
		}
		/// <summary>
		/// Whether to show the 'neighbor' numbers
		/// </summary>
		public bool ShowNumbers
		{
			get => chkNumbers.Checked;
			set => chkNumbers.Checked = value;
		}
		/// <summary>
		/// Whether to show the grid lines
		/// </summary>
		public bool ShowGrid
		{
			get => chkGrid.Checked;
			set => chkGrid.Checked = value;
		}
		/// <summary>
		/// Whether to show the HUD
		/// </summary>
		public bool ShowHUD
		{
			get => chkHud.Checked;
			set => chkHud.Checked = value;
		}
		/// <summary>
		/// Whether to wrap edges around to other side
		/// </summary>
		public bool WrapEdges
		{
			get => chkWrap.Checked;
			set => chkWrap.Checked = value;
		}
	}
}
