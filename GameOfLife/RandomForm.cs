﻿using System;
using System.Windows.Forms;

namespace GameOfLife
{
	public partial class RandomForm : Form
	{
		public RandomForm()
		{
			InitializeComponent();
		}
		/// <summary>
		/// Percent of 'Living' cells
		/// </summary>
		public decimal Living
		{
			get => txtLiving.Value;
			set => txtLiving.Value = value;
		}
		/// <summary>
		/// The seed for the pseudo-random generator
		/// </summary>
		public decimal Seed
		{
			get => txtSeed.Value;
			set => txtSeed.Value = value;
		}
	}
}
