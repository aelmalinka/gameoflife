﻿using System;
using System.IO;

namespace GameOfLife
{
	class UniverseReader : StreamReader
	{
		/// <summary>
		/// Insantiate a UniverseReader object
		/// </summary>
		/// <param name="file">The path of the file to read from</param>
		public UniverseReader(string file) :
			base(file)
		{}
		/// <summary>
		/// Read from the file into the CellPanel object
		/// </summary>
		/// <param name="cells">The CellPanel object to read into</param>
		/// <param name="resize">Wheter to resize the CellPanel object to match the file (import vs open)</param>
		public void Read(CellPanel cells, bool resize = true)
		{
			int w, h;
			(w, h) = ReadSize();

			if (resize)
				cells.Reset(w, h);

			for (int y = Math.Max(cells.Height / 2 - h / 2, 0); y < cells.Height && !EndOfStream; y++)
			{
				string l = "!";
				while (l.StartsWith("!"))
					l = ReadLine();

				for (int x = Math.Max(cells.Width / 2 - w / 2, 0), i = 0; x < cells.Width && i < l.Length; x++, i++)
				{
					if (l[i] == 'O')
						cells[x, y] = true;
					else if (l[i] == '.')
						; // 2018-12-12 AMR NOTE: Specs ask to not clear field, not setting dead cells explicitly (they will be false on reset (resized) field)
					else
						throw new FormatException("Unexpected character found");
				}
			}
		}
		/// <summary>
		/// The size of the 'Universe' in the contained file
		/// </summary>
		/// <returns>Width/Height of the Universe</returns>
		(int, int) ReadSize()
		{
			(int, int) ret = (0, 0);
			while (!EndOfStream)
			{
				string l = ReadLine();
				if (l[0] == '!')
					continue;
				++ret.Item2;
				if (l.Length > ret.Item1)
					ret.Item1 = l.Length;
			}
			// 2018-12-07 AMR NOTE: Back to the front
			BaseStream.Seek(0, SeekOrigin.Begin);
			return ret;
		}
	}
}
