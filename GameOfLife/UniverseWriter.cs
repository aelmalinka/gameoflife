﻿using System;
using System.IO;
using System.Text;

namespace GameOfLife
{
	class UniverseWriter : StreamWriter
	{
		/// <summary>
		/// Instantiate a new UniverseWriter
		/// </summary>
		/// <param name="file">The path of the file to write to</param>
		public UniverseWriter(string file) :
			base(file)
		{
			name = file;
		}
		/// <summary>
		/// Write the CellPanel object into the file
		/// </summary>
		/// <param name="cells">The CellPanel object to write</param>
		public void Write(CellPanel cells)
		{
			WriteLine($"!Name: {name}");
			WriteLine($"!When: {DateTime.Now}");
			WriteLine($"!Size: {cells.Width}x{cells.Height}");

			for (int y = 0; y < cells.Height; y++)
			{
				StringBuilder builder = new StringBuilder(cells.Width);
				for (int x = 0; x < cells.Width; x++)
				{
					if (cells[x, y])
						builder.Append('O');
					else
						builder.Append('.');
				}
				WriteLine(builder.ToString());
			}
		}
		readonly string name;
	}
}
